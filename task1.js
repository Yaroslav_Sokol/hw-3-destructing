// Завдання 1
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

// Зробив два варіанти, не знаю який краще
const listClients1 = new Set(clients1.concat(clients2)); // Перший варіант
const listClients2 = [...new Set([...clients1,  ...clients2])]; // Другий варіант
console.log(listClients1);
console.log(listClients2);