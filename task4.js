// Завдання 4
const satoshi2020 = {
  name: 'Nick',
  surname: 'Sabo',
  age: 51,
  country: 'Japan',
  birth: '1979-08-21',
  location: {
    lat: 38.869422, 
    lng: 139.876632
  }
}

const satoshi2019 = {
  name: 'Dorian',
  surname: 'Nakamoto',
  age: 44,
  hidden: true,
  country: 'USA',
  wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
  browser: 'Chrome'
}

const satoshi2018 = {
  name: 'Satoshi',
  surname: 'Nakamoto', 
  technology: 'Bitcoin',
  country: 'Japan',
  browser: 'Tor',
  birth: '1975-04-05'
}

const { name, surname, age, country, birth, lat, lng, hidden, wallet, browser, technology  } = {...satoshi2018, ...satoshi2019, ...satoshi2020};

// Виведу все в один рядок, аби не прописувать велику кількість console.log;
console.log(`Імя: ${name}, Прізвище: ${surname}, Вік: ${age}, Країна: ${country}, Гаманець: ${wallet}, Hidden: ${hidden}, Браузер: ${browser}, Технология: ${technology}`);
