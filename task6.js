const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

// Перший варіант
const newEmployee2 = {
    ...employee,
    age: 0,
    salary: 0
}

// Другий варіант
const newProperties = {
    age: 0,
    salary: 0
}
const newEmployee = {...employee, ...newProperties};

console.log(newEmployee);
console.log(newEmployee2);