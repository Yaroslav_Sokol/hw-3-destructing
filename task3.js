// Завдання 3
const user1 = {
    name: "John",
    years: 30
  };
  
  const {name: імя, years: вік, isAdmin = false} = user1;
  
  console.log(імя);
  console.log(вік);
  console.log(isAdmin);